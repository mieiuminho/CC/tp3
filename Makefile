#==============================================================================
SHELL      = zsh
#------------------------------------------------------------------------------
FILTERS    = -F pandoc-crossref -F pandoc-include-code
OPTIONS    = --template=styles/template.tex $(FILTERS)
CONFIG     = --metadata-file config.yml
BIB        = --filter pandoc-citeproc --bibliography=references.bib
#------------------------------------------------------------------------------
SRC        = $(shell ls $(SRC_DIR)/**/*.md)
SRC_DIR    = sections
REPORT     = report
#------------------------------------------------------------------------------
RESORC_DIR = resources
SUBMISSION = cc-dns-PL5-G6
#==============================================================================

pdf:
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SRC) -o $(REPORT).pdf

tar: pdf
	@mkdir -p $(SUBMISSION)/report
	@cp $(REPORT).pdf $(SUBMISSION)/report
	@cp -r $(RESORC_DIR)/primario $(SUBMISSION)
	@cp -r $(RESORC_DIR)/secundario $(SUBMISSION)
	@tar czvf $(SUBMISSION).tgz $(SUBMISSION)

clean:
	@echo "Cleaning..."
	@-cat .art/maid.ascii
	@-rm $(REPORT).pdf
	@-rm -rf $(SUBMISSION) $(SUBMISSION).tgz
	@echo "...✓ done!"

