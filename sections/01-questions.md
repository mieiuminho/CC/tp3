# Consultas ao serviço de nomes DNS

## Questões e Respostas

### a)

**Qual o conteúdo do ficheiro `/etc/resolv.conf` e para que serve essa
informação?**

```bash
$ cat /etc/resolv.conf
# This file is managed by man:systemd-resolved(8). Do not edit.
#
# This is a dynamic resolv.conf file for connecting local clients to the
# internal DNS stub resolver of systemd-resolved. This file lists all
# configured search domains.
#
# Run "systemd-resolve --status" to see details about the uplink DNS servers
# currently in use.
#
# Third party programs must not access this file directly, but only through the
# symlink at /etc/resolv.conf. To manage man:resolv.conf(5) in a different way,
# replace this symlink by a static file or a different symlink.
#
# See man:systemd-resolved.service(8) for details about the supported modes of
# operation for /etc/resolv.conf.

nameserver 127.0.0.53
options edns0
search home
```

Este ficheiro contém os servidores DNS por omissão para resolução de nomes de
domínio e endereços IP.

### b)

**Os servidores www.sapo.pt. e www.yahoo.com. têm endereços IPv6? Se sim,
quais?**

- `www.sapo.pt`:

```
2001:8a0:2102:c:213:13:146:142
```

- `www.yahoo.com`:

```
Name:	atsv2-fp-shed.wg1.b.yahoo.com

Address: 2a00:1288:110:1c::3

Name:	atsv2-fp-shed.wg1.b.yahoo.com
Address: 2a00:1288:110:1c::4
```

\newpage

### c)

**Quais os servidores de nomes definidos para os domínios: “uminho.pt.”, “pt.” e
“.”?**

```bash
$ dig uminho.pt.
; <<>> DiG 9.16.0 <<>> uminho.pt.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41612
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;uminho.pt.			IN	A

;; AUTHORITY SECTION:
uminho.pt.		150	IN	SOA	dns.uminho.pt. servicos.scom.uminho.pt. 2020032016 \
                                                    14400 7200 1209600 300

;; Query time: 9 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: ter mar 24 09:52:59 WET 2020
;; MSG SIZE  rcvd: 92
```

Assim, o servidor de resoluão de nomes para _uminho.pt._ é `dns.uminho.pt`.

```bash
$ dig pt.
 <<>> DiG 9.16.0 <<>> pt.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 65395
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;pt.				IN	A

;; AUTHORITY SECTION:
pt.			299	IN	SOA	curiosity.dns.pt. request.dns.pt. 2020032419 21600 7200 \
                                                                  2592000 300

;; Query time: 19 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: ter mar 24 09:53:14 WET 2020
;; MSG SIZE  rcvd: 89
```

Assim, o servidor de resolução de nomes para _pt._ é `curiosity.dns.pt`.

\newpage

```bash
$ dig .
; <<>> DiG 9.16.0 <<>> .
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 17120
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;.				IN	A

;; AUTHORITY SECTION:
.			1687	IN	SOA	a.root-servers.net. nstld.verisign-grs.com. 2020032400 \
                                                        1800 900 604800 86400

;; Query time: 19 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: ter mar 24 09:53:30 WET 2020
;; MSG SIZE  rcvd: 103
```

Assim, o servidor de resolução de nomes para _._ é `a.root-servers.net`.

### d)

**Existe o domínio nice.software.? Será que nice.software. é um host ou um
domínio?**

É um _host_ uma vez que tem endereço de IP: `213.212.81.71`.

\newpage

### e)

**Qual é o servidor DNS primário definido para o domínio `msf.org.`? Este
servidor primário (_master_) aceita _queries_ recursivas? Porquê?**

```bash
$ host -t soa msf.org
msf.org has SOA record ns1.dds.nl. postmaster.msf.org. 1407464621 16384 2048 \
                                                              1048576 2560
```

O servidor de DNS primário é `ns1.dds.nl`.

```bash
$ dig ns1.dds.nl
; <<>> DiG 9.11.3-1ubuntu1.11-Ubuntu <<>> ns1.dds.nl
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64086
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;ns1.dds.nl.			IN	A

;; ANSWER SECTION:
ns1.dds.nl.		86400	IN	A	91.142.253.70

;; Query time: 68 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Tue Mar 24 10:16:57 WET 2020
;; MSG SIZE  rcvd: 55
```

Tal como se pode ver na secção das flags está presente o identificador `ra` que
significa _recursion available_. Pelo que é possível fazer _queries_ recursivas.

### f)

**Obtenha uma resposta “autoritativa” para a questão anterior.**

```bash
$ nslookup
> set q=NS
> msf.org
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
msf.org	nameserver = ns1.dds.nl.
msf.org	nameserver = ns2.dds.eu.
msf.org	nameserver = ns4.dds-city.com.
msf.org	nameserver = ns3.dds.amsterdam.

Authoritative answers can be found from:
```

Uma vez que a lista de servidores autoritativos está vazia não foi possível
obter uma resposta autoritativa.

### g)

**Onde são entregues as mensagens de correio eletrónico dirigidas aos
presidentes `marcelo@presidencia.pt` e `bolsonaro@casacivil.gov.br`?**

```bash
$ host presidencia.pt
presidencia.pt has address 192.162.17.10
presidencia.pt mail is handled by 50 mail1.presidencia.pt.
presidencia.pt mail is handled by 10 mail2.presidencia.pt.
```
As mensagens de correio eletrónico para `marcelo@presidencia.pt` são entregues
nos servidores `mail1.presidencia.pt` e `mail2.presidencia.pt`,
preferencialmente no primeiro uma vez que tem grau de preferência superior ao
segundo (50 > 10).

```bash
$ host casacivil.gov.br
casacivil.gov.br has address 170.246.255.25
casacivil.gov.br mail is handled by 5 esa01.presidencia.gov.br.
casacivil.gov.br mail is handled by 10 esa01.presidencia.gov.br.
```

As mensagens de correio eletrónico para `bolsonaro@casacivil.gov.br` são
entregues nos servidores `esa01.presidencia.gov.br` e
`esa01.presidencia.gov.br`, preferencialmente no segundo uma vez que tem grau de
preferência superior ao primeiro (5 < 10).

### h)

**Que informação é possível obter, via DNS, acerca de `whitehouse.gov`?**

```bash
$ dig whitehouse.gov
; <<>> DiG 9.11.3-1ubuntu1.11-Ubuntu <<>> whitehouse.gov
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7948
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;whitehouse.gov.			IN	A

;; ANSWER SECTION:
whitehouse.gov.		20	IN	A	184.27.2.2

;; Query time: 35 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Tue Mar 24 10:51:24 WET 2020
;; MSG SIZE  rcvd: 59
```

\newpage

```bash
$ host whitehouse.gov
whitehouse.gov has address 184.27.2.2
whitehouse.gov has IPv6 address 2a02:26f0:e0:284::2add
whitehouse.gov has IPv6 address 2a02:26f0:e0:28c::2add
```

```bash
$ nslookup whitehouse.gov
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
Name:	whitehouse.gov
Address: 69.192.66.35
Name:	whitehouse.gov
Address: 2a02:26f0:e0:28c::2add
Name:	whitehouse.gov
Address: 2a02:26f0:e0:29c::2add
```

É possível obter os endereços IPv4 e IPv6. As opções ativas no servidor são as
seguintes: `qr`, `rd` (_recursive desirable_) e `ra` (_recursive available_).

### i)

**Consegue interrogar o DNS sobre o endereço IPv6 `2001:690:a00:1036:1113::247`
usando algum dos clientes DNS? Que informação consegue obter? Supondo que teve
problemas com esse endereço, consegue obter um contacto do responsável por esse
IPv6?**


```bash
$ dig 2001:690:a00:1036:1113::247
; <<>> DiG 9.16.0 <<>> 2001:690:a00:1036:1113::247
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 34220
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;2001:690:a00:1036:1113::247.	IN	A

;; AUTHORITY SECTION:
.			2208	IN	SOA	a.root-servers.net. nstld.verisign-grs.com. 2020032400 \
                                                          1800 900 604800 86400

;; Query time: 69 msec
;; SERVER: 192.168.1.254#53(192.168.1.254)
;; WHEN: ter mar 24 11:06:34 WET 2020
;; MSG SIZE  rcvd: 131
```

\newpage

```bash
$ nslookup 2001:690:a00:1036:1113::247
7.4.2.0.0.0.0.0.0.0.0.0.3.1.1.1.6.3.0.1.0.0.a.0.0.9.6.0.1.0.0.2.ip6.arpa	name \
                                                                    = fccn.pt.

Authoritative answers can be found from:
6.3.0.1.0.0.a.0.0.9.6.0.1.0.0.2.ip6.arpa	nameserver = ns02.fccn.pt.
6.3.0.1.0.0.a.0.0.9.6.0.1.0.0.2.ip6.arpa	nameserver = ns01.fccn.pt.
```

É possível obter qual o domínio (www.fccn.pt).

### j)

**Os secundários usam um mecanismo designado por “Transferência de zona” para se
atualizarem automaticamente a partir do primário, usando os parâmetros definidos
no Record do tipo SOA do domínio. Descreve sucintamente esse mecanismo com base
num exemplo concreto (ex: `di.uminho.pt` ou o domínio `cc.pt` que vai ser criado
na topologia virtual).**

Transferência de zona DNS é uma query DNS do tipo IXFR ou AXFR. Esta query é
usada para replicar uma porção contígua (zona) ou a totalidade da base de dados
DNS do servidor que a recebe. A transferência é feita, através de uma ligação
TCP, começando pela verificação do preâmbulo que contém um número de série. Esta
verificação determina se a transferência tem de facto de ocorrer pois, se o
número de série for igual ou inferior ao do servidor que envia o pedido 7 de
transferência de zona, a transferência não ocorre dado que este contém uma
versão da base de dados igual ou mais recente.

