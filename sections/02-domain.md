# Instalação, configuração e teste de um domínio CC.PT

## Topologia

![Topologia CORE](figures/topologia.png)


## Servidores

![Servidor Primário, Serv1.cc.pt, 10.3.3.1](figures/hermes-start.png)

![Servidor Secundário, Hermes.cc.pt, 10.4.4.1](figures/hermes-start.png)

## Queries

![Cliente Alfa, LAN2](figures/queries-alfa.png)

![Cliente Omega, LAN2](figures/queries-omega.png)

